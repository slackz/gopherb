# coding: utf-8
require 'curses'
require 'socket'
require_relative 'ncurses'
require_relative '../logging'

class Gopherb
  class Client
    KEYS = {
      ctrl_p: 16,
      ctrl_n: 14,
      enter: 10
    }
    CLIENT_QUIT = /^q$/i
    SERVER_QUIT = /^\./
    RESOURCE_TYPES = {
      '0' => :document,
      '1' => :directory
    }

    def initialize(host, port)
      @host, @port = host, port
      @mode = :nav # vs "view"
    end

    def nav_mode?
      @mode == :nav
    end

    def run
      @logger = Logging.new('client')
      Gopherb::Ncurses.init
      @parsed_lines = server_run
      @menu = create_menu(@parsed_lines)

      # toplevel loop for making requests to server
      while(true)
        cmd = Curses.getch

        begin
          case cmd
          when "g", ":"
            @host, @port = Gopherb::Ncurses.prompt, 70
            @mode = :nav

            Gopherb::Ncurses.reset
            @parsed_lines = server_run
            @menu = create_menu(@parsed_lines)
          when Curses::KEY_UP, "k", KEYS[:ctrl_p]
            if nav_mode?
              @menu.up_item
              @menu_idx -= 1 unless @menu_idx.zero?
            end
          when Curses::KEY_DOWN, "j", KEYS[:ctrl_n]
            if nav_mode?
              @menu.down_item
              @menu_idx += 1 unless (@menu_idx + 1 == @parsed_lines.count)
            end
          when Curses::KEY_ENTER, KEYS[:enter]
            selected = @parsed_lines[@menu_idx]
            @host, @port  = selected[:host], selected[:port]
            Gopherb::Ncurses.reset
            @parsed_lines = server_run("#{selected[:selector]}\n", selected[:type])
            @logger.log(@parsed_lines)
            if selected[:type] == :directory
              @logger.log("selected: #{selected}")
              @menu = create_menu(@parsed_lines)
            else
              i = 0
              @mode = :view
              @menu.unpost
              @parsed_lines.each do |l|
                #Curses.nonl
                @logger.log(l)
                Curses.setpos(i, 0)
                Curses.addstr(l.gsub("\r\n", "\n"))
                i += 1
              end
            end
            Curses.refresh
          when "q"
            break
          else
            @logger.log("curr cmd: #{cmd}")
          end
        rescue Curses::RequestDeniedError
        end
      end
      @menu.unpost
    end

    private

    def create_socket(host, port = 70)
      TCPSocket.new(host, port)
    end

    def create_menu(parsed_lines)
      @menu_idx = 0 # to keep track of current item
      Gopherb::Ncurses.create_menu(parsed_lines)
    end

    def server_run(cmd = "\n", type = :directory)
      server = create_socket(@host, @port.to_i)
      server.puts(cmd)

      results = []
      # get response from server
      while(line = server.gets)
        if (type == :directory)
          next if line !~ /^[0-9]/
          results << parse(line)
        else
          @logger.log('should be here')
          break if line =~ SERVER_QUIT
          results << line
        end
      end

      server.close
      results
    end

    def parse(line)
      prefix, selector, host, port = line.split("\t").map do |str|
        str.strip.dup.force_encoding(Encoding::UTF_8)
      end

      regex_res = prefix.match /^(\d+)(.*)$/
      type, description = regex_res[1], regex_res[2]
      { host: host, port: port, selector: selector,
        description: description, type: RESOURCE_TYPES[type] }
    end
  end
end

#Gopherb::Client.new('localhost', 7070).run
Gopherb::Client.new('gopher.orghub.co', 70).run
