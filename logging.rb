require 'logger'

class Logging

  def initialize(name)
    fname   = File.join('logs', "#{name}.log")
    @logger = Logger.new(fname)
    @logger.level = Logger::WARN
  end

  def log(msg)
    @logger.warn(msg)
  end

end
